import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
    <Nav/>
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />}/>
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList />}></Route>
          <Route path="new" element={<AttendeeForm />}></Route>
        </Route>
        <Route path="location">
          <Route path="new" element={<LocationForm />}></Route>
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />}></Route>
        </Route>
      </Routes>
    </BrowserRouter>

    </>
  );
}

export default App;

// <Nav />
// <div className="container">
//   <AttendeeForm />
//   {/* {<ConferenceForm />} */}
//   {/* <LocationForm /> */}
//   {/* <AttendeesList attendees={props.attendees} /> */}
// </div>