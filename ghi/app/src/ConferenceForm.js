import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [ locations, setLocations ] = useState([]);
    const [ formData, setFormData ] = useState({
        name: "",
        starts: "",
        ends: "",
        description: "",
        max_presentations: "",
        max_attendees: "",
        location: ""
    })

    const handleFormChange = (e) => {
      setFormData({
        ...formData,
        [e.target.name]: e.target.value
      });
    }

    const handleSubmit = async (e) => {
      e.preventDefault();
      console.log(formData);

      const conferenceUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(formData),
          headers: {
              'Content-Type': 'application/json',
          },
      };
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
          const newConference = await response.json();
          console.log(newConference)
          setFormData({
            name: "",
            starts: "",
            ends: "",
            description: "",
            max_presentations: "",
            max_attendees: "",
            location: ""
          });
      }
    }

    const fetchData = async () => {
      const url = 'http://localhost:8000/api/locations/';
      const response = await fetch(url);
      if (response.ok) {
          const data = await response.json();
          console.log(data)
          setLocations(data.locations);
      }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="mb-3">
                <label htmlFor="starts" className="form-label">Starts</label>
                <input onChange={handleFormChange} value={formData.starts} placeholder="Starts" className="form-control" required type="date" name="starts" id="starts"/>
              </div>
              <div className="mb-3">
                <label htmlFor="ends" className="form-label">Ends</label>
                <input onChange={handleFormChange} value={formData.ends} placeholder="Ends" className="form-control" required type="date" id="ends" name="ends"></input>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleFormChange} value={formData.description} required type="text" name="description" id="description" className="form-control" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.max_presentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <label htmlFor="max_attendees">Room count</label>
                <input onChange={handleFormChange} value={formData.max_attendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
              </div>
              <div className="mb-3">
                <select onChange={handleFormChange} value={formData.location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {
                    locations.map(l => <option key={l.id} value={l.id}>{l.name}</option>)
                  }
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm;