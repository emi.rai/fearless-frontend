import React, { useEffect, useState } from 'react';

function AttendeeForm () {
    const [ conferences, setConferences ] = useState([]);
    const [ formData, setFormData ] = useState({
        name: "",
        email: "",
        conference: "",
    });
    let spinnerClasses = "d-flex justify-content-center mb-3";
    let dropdownClasses = "mb-3 d-none";
    if (conferences.length > 0) {
        spinnerClasses = "d-flex justify-content-center mb-3 d-none";
        dropdownClasses = "mb-3";
    }
    const handleFormChange = (e) => {
        setFormData(
            {
                ...formData,
                [e.target.name]: e.target.value
            }
        );
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        console.log("formData ", formData);
        const conferenceUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = fetch(conferenceUrl, fetchConfig);
        console.log("hello");
        if (response.ok) {
            console.log("hello2");
            const newAttendee = await response.json();
            console.log(newAttendee);
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
        <div className="my-5">
          <div className="row">
            <div className="col col-sm-auto">
              <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="logo"/>
            </div>
            <div className="col">
              <div className="card shadow">
                <div className="card-body">
                  <form onSubmit={handleSubmit} id="create-attendee-form">
                    <h1 className="card-title">It's Conference Time!</h1>
                    <p className="mb-3">
                      Please choose which conference
                      you'd like to attend.
                    </p>
                    <div className={spinnerClasses} id="loading-conference-spinner">
                      <div className="spinner-grow text-secondary" role="status">
                        <span className="visually-hidden">Loading...</span>
                      </div>
                    </div>
                    <div className={dropdownClasses}>
                      <select onChange={handleFormChange} name="conference" id="conference" className="form-select" required>
                        <option value="">Choose a conference</option>
                        {
                            conferences.map(c => <option key={c.id} value={c.href}>{c.name}</option>)
                        }
                      </select>
                    </div>
                    <p className="mb-3">
                      Now, tell us about yourself.
                    </p>
                    <div className="row">
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleFormChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                          <label htmlFor="name">Your full name</label>
                        </div>
                      </div>
                      <div className="col">
                        <div className="form-floating mb-3">
                          <input onChange={handleFormChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                          <label htmlFor="email">Your email address</label>
                        </div>
                      </div>
                    </div>
                    <button className="btn btn-lg btn-primary">I'm going!</button>
                  </form>
                  <div className="alert alert-success d-none mb-0" id="success-message">
                    Congratulations! You're all signed up!
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}

export default AttendeeForm;