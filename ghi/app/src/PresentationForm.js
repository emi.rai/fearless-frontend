import React, { useEffect, useState } from "react";

function PresentationForm() {
    const [conferences, setConferences ] = useState([]);
    const [ formData, setFormData ] = useState({
        presenter_name: "",
        presenter_email: "",
        company_name: "",
        title: "",
        synopsis: "",
        conference: "",
    })

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
      }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const confId = e.target.conference.value;

        const presentationUrl = `http://localhost:8000/api/conferences/${confId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation)
            setFormData({
                presenter_name: "",
                presenter_email: "",
                company_name: "",
                title: "",
                synopsis: "",
                conference: "",
            })
        }
      }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.presenter_name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" />
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="presenter_email" className="form-label">Presenter email</label>
                            <input onChange={handleFormChange} placeholder="Presenter email" value={formData.presenter_email} className="form-control" required type="text" name="presenter_email" id="presenter_email" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="company_name" className="form-label">Company Name</label>
                            <input onChange={handleFormChange} value={formData.company_name} placeholder="Company name" className="form-control" required type="text" id="company_name" name="company_name" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="title">Title</label>
                            <input onChange={handleFormChange} value={formData.title} placeholder="Title" className="form-control" required type="text" id="title" name="title" />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis" className="form-label">Synopsis</label>
                            <textarea onChange={handleFormChange} value={formData.synopsis} required type="text" name="synopsis" id="synopsis" className="form-control" rows="3"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.conference} id="conference" name="conference" className="form-select">
                            <option value="">Choose a conference</option>
                            {
                                conferences.map(c => <option key={c.id} value={c.id}>{c.name}</option>)
                            }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
      </>
    )
}

export default PresentationForm;