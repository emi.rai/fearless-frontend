function createCard(name, description, pictureUrl, start, end, location_name) {
    return `
        <div class="card shadow mb-2 bg-white rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location_name}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer">
                ${start} - ${end}
            </div>
            </div>
        </div>
      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        const errors = document.querySelector(".error");
        const html = `<div class="alert alert-primary" role="alert">Response not ok.</div>`
        errors.innerHTML += html;
      } else {
        const data = await response.json();

        let count = 0;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const start = new Date(details.conference.starts).toLocaleDateString();

            const end = new Date(details.conference.ends).toLocaleDateString();
            const location_name = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, start, end, location_name);
            count ++;
            let col = '';
            if (count === 1) {
                col = '<div class="row align-items-start">'
                const column = document.querySelector('.first');
                column.innerHTML += html;
            } else if (count === 2) {
                col = '<div class="row align-items-center">'
                const column = document.querySelector('.second');
                column.innerHTML += html;
            } else if (count === 3) {
                col = '<div class="row align-items-end">'
                const column = document.querySelector('.third');
                column.innerHTML += html;
                count = 0;
            }

          }
        }
      }
    } catch (e) {
      const errors = document.querySelector(".error");
      const html = `<div class="alert alert-primary" role="alert">Error</div>`
      errors.innerHTML += html;
    }

  });

// window.addEventListener('DOMContentLoaded', async () => {
//   const url = 'http://localhost:8000/api/conferences/';

//   try {
//     const response = await fetch(url);
//     if (!response.ok) {
//     throw new Error('Response not ok');
//     } else {
//       const data = await response.json();
//       const conference = data.conferences[0];
//       const nameTag = document.querySelector('.card-title');
//       nameTag.innerHTML = conference.name;


//       const detailUrl = `http://localhost:8000${conference.href}`;
//       const detailResponse = await fetch(detailUrl);
//       if (detailResponse.ok) {
//         const details = await detailResponse.json();

//         const description = details.conference.description;
//         const descTag = document.querySelector('.card-text');
//         descTag.innerHTML = description;

//         const picture = details.conference.location.picture_url;
//         const pictTag = document.querySelector('.card-img-top');
//         pictTag.src = picture;
//       }

//     }
//   } catch (e) {
//     console.error('error', e);
//   }

// });
