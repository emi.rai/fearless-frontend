window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const select = document.getElementById('conference');

    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            // console.log(data);

            for (const conference of data.conferences) {
                const option = document.createElement('option');
                option.innerHTML = conference.name;
                const confHref = conference.href;
                const confId = confHref[confHref.length - 2];
                option.value = confId;
                select.appendChild(option);
            }
        }
    } catch (e) {
        console.error("Error loading conference ", e);
    }

    const formTag = document.getElementById('create-presentation-form');

    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const selectTag = document.getElementById('conference');
        const confId = selectTag.value;
        console.log(confId);
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        console.log(selectTag);
        const presentationUrl =
            `http://localhost:8000/api/conferences/${confId}/presentations/`;
        console.log(presentationUrl);
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-type': 'application/json',
            }
        }
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset;
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    })
})