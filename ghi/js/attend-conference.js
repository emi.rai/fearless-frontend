window.addEventListener('DOMContentLoaded', async () => {

    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      const loadingIcon = document.getElementById('loading-conference-spinner');
      loadingIcon.classList.add('d-none');
      const select = document.getElementById('conference');
      select.classList.remove('d-none');
    }
    const attendeeForm = document.getElementById('create-attendee-form');

    attendeeForm.addEventListener('submit', async event => {
        event.preventDefault();

        const attendeeData = new FormData(attendeeForm);
        const json = JSON.stringify(Object.fromEntries(attendeeData));
        const jsonObj = JSON.parse(json)
        const conferenceUrl = jsonObj.conference;
        const attendeeUrl = `http://localhost:8001${conferenceUrl}attendees/`;
        console.log(attendeeUrl);
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-type': 'application/json'
            }
        }
        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            attendeeForm.reset();
            // const newAttendee = await response.json();
        }
        attendeeForm.classList.add('d-none');
        const alert = document.querySelector('#success-message');
        alert.classList.remove('d-none');
    })
});