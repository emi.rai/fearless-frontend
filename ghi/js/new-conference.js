window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
    const select = document.getElementById("location");

    try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);

            for (const location of data.locations) {
                const option = document.createElement('option');
                option.innerHTML = location.name;
                option.value = location.id;
                select.appendChild(option);
            }
        }
    } catch (e) {
        console.error("Error ", e);
    }

    const formTag = document.getElementById('create-conference-form');

    formTag.addEventListener('submit', async event => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-type': 'application/json',
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        }
    })
})